# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_09_17_150237) do
  create_table "cms_blog_translations", force: :cascade do |t|
    t.integer "cms_blog_id"
    t.string "locale"
    t.string "title"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["cms_blog_id"], name: "index_cms_blog_translations_on_cms_blog_id"
  end

  create_table "cms_blogs", force: :cascade do |t|
    t.string "slug"
    t.boolean "published", default: false
    t.boolean "is_public", default: false
    t.boolean "requires_login", default: false
    t.integer "row_order"
    t.datetime "updated_on", precision: nil
    t.datetime "created_on", precision: nil
    t.integer "account_id"
    t.integer "lock_version", default: 0, null: false
    t.boolean "comments_allowed"
    t.string "header_image"
    t.boolean "requires_subscription", default: false
    t.integer "owner_id"
    t.string "owner_type"
    t.string "image_email_header"
    t.index ["account_id"], name: "index_cms_blogs_on_account_id"
    t.index ["owner_id"], name: "index_cms_blogs_on_owner_id"
    t.index ["published"], name: "cms_blogs_published_index"
    t.index ["slug"], name: "blogname_key"
  end

  create_table "cms_contentitem_translations", force: :cascade do |t|
    t.integer "cms_contentitem_id"
    t.string "locale"
    t.text "content"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["cms_contentitem_id"], name: "index_072c3a76f87c96e2f91149eccd2a283dca2b613f"
  end

  create_table "cms_contentitems", force: :cascade do |t|
    t.integer "cms_page_id"
    t.string "itemtype", limit: 30, default: "textile"
    t.string "container", limit: 30
    t.integer "row_order"
    t.boolean "enable_cache", default: true, null: false
    t.datetime "created_on", precision: nil
    t.datetime "updated_on", precision: nil
    t.integer "lock_version", default: 0
    t.integer "account_id"
    t.index ["account_id"], name: "index_cms_contentitems_on_account_id"
    t.index ["cms_page_id"], name: "cms_contentitems_cms_page_id_index"
  end

  create_table "cms_media_file_translations", force: :cascade do |t|
    t.integer "cms_media_file_id"
    t.string "locale"
    t.string "title"
    t.string "description"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["cms_media_file_id"], name: "index_cms_media_file_translations_on_cms_media_file_id"
  end

  create_table "cms_media_files", force: :cascade do |t|
    t.string "media"
    t.integer "media_file_size"
    t.string "media_content_type"
    t.string "folder"
    t.boolean "generate_retina"
    t.integer "user_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.index ["account_id"], name: "index_cms_media_files_on_account_id"
    t.index ["media", "folder", "account_id"], name: "index_media_folder_account_id", unique: true
    t.index ["user_id"], name: "index_cms_media_files_on_user_id"
  end

  create_table "cms_page_translations", force: :cascade do |t|
    t.integer "cms_page_id"
    t.string "locale"
    t.string "title"
    t.string "menutitle"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.text "summary"
    t.index ["cms_page_id"], name: "index_cms_page_translations_on_cms_page_id"
  end

  create_table "cms_pages", force: :cascade do |t|
    t.string "slug", limit: 50, default: "", null: false
    t.boolean "published", default: false, null: false
    t.string "template", limit: 50, default: "", null: false
    t.string "link", default: "", null: false
    t.string "menuimage"
    t.boolean "requires_login", default: false
    t.string "ancestry"
    t.integer "ancestry_depth", default: 0
    t.datetime "updated_on", precision: nil
    t.datetime "created_on", precision: nil
    t.integer "lock_version", default: 0, null: false
    t.integer "account_id"
    t.boolean "requires_subscription", default: false
    t.boolean "is_public", default: true
    t.integer "row_order"
    t.string "featured_image"
    t.string "header_image"
    t.boolean "welcome_page", default: false
    t.index ["account_id"], name: "index_cms_pages_on_account_id"
    t.index ["ancestry"], name: "index_cms_pages_on_ancestry"
    t.index ["ancestry_depth"], name: "index_cms_pages_on_ancestry_depth"
    t.index ["published"], name: "cms_pages_published_index"
    t.index ["slug"], name: "pagename_key"
  end

  create_table "cms_post_translations", force: :cascade do |t|
    t.integer "cms_post_id"
    t.string "locale"
    t.string "title"
    t.text "content"
    t.text "summary"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["cms_post_id"], name: "index_cms_post_translations_on_cms_post_id"
  end

  create_table "cms_posts", force: :cascade do |t|
    t.string "slug"
    t.integer "cms_blog_id"
    t.string "featured_image"
    t.datetime "published_on", precision: nil
    t.datetime "updated_on", precision: nil
    t.datetime "created_on", precision: nil
    t.integer "account_id"
    t.integer "lock_version", default: 0, null: false
    t.datetime "notification_sent_on", precision: nil
    t.integer "comments_count", default: 0
    t.boolean "comments_allowed"
    t.index ["account_id"], name: "index_cms_posts_on_account_id"
    t.index ["cms_blog_id"], name: "index_cms_posts_on_cms_blog_id"
    t.index ["published_on"], name: "cms_posts_published_index"
    t.index ["slug"], name: "postname_key"
  end

  create_table "cms_snippet_translations", force: :cascade do |t|
    t.integer "cms_snippet_id"
    t.string "locale"
    t.text "content"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["cms_snippet_id"], name: "index_cms_snippet_translations_on_cms_snippet_id"
  end

  create_table "cms_snippets", force: :cascade do |t|
    t.string "itemtype", default: "markdown"
    t.string "slug"
    t.string "description"
    t.boolean "enable_cache", default: true, null: false
    t.boolean "published", default: true, null: false
    t.datetime "created_on", precision: nil
    t.datetime "updated_on", precision: nil
    t.integer "lock_version", default: 0
    t.integer "account_id"
    t.index ["account_id"], name: "index_cms_snippets_on_account_id"
    t.index ["slug"], name: "index_cms_snippets_on_slug"
  end

  create_table "core_accounts", force: :cascade do |t|
    t.string "company_name"
    t.string "contact_email"
    t.string "domain"
    t.string "account_prefix"
    t.integer "default_site_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "next_invoice_num", default: 1000
    t.index ["default_site_id"], name: "index_core_accounts_on_default_site_id"
  end

  create_table "core_activities", force: :cascade do |t|
    t.integer "account_id"
    t.integer "user_id"
    t.string "browser"
    t.string "session_id"
    t.string "ip_address"
    t.string "controller"
    t.string "action"
    t.string "params"
    t.string "slug"
    t.string "lesson"
    t.datetime "created_at", precision: nil
    t.index ["account_id"], name: "index_core_activities_on_account_id"
    t.index ["user_id"], name: "index_core_activities_on_user_id"
  end

  create_table "core_addresses", force: :cascade do |t|
    t.string "line1"
    t.string "line2"
    t.string "city"
    t.string "state"
    t.string "zip"
    t.string "country_code", limit: 2
    t.integer "addressable_id"
    t.string "addressable_type"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["addressable_id"], name: "index_core_addresses_on_addressable_id"
    t.index ["addressable_type", "addressable_id"], name: "index_core_addresses_on_addressable_type_and_addressable_id", unique: true
  end

  create_table "core_categories", force: :cascade do |t|
    t.string "type"
    t.integer "row_order"
    t.integer "account_id"
    t.datetime "created_on", precision: nil
    t.datetime "updated_on", precision: nil
    t.index ["account_id"], name: "index_core_categories_on_account_id"
  end

  create_table "core_category_translations", force: :cascade do |t|
    t.integer "core_category_id"
    t.string "locale"
    t.string "name"
    t.string "description"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["core_category_id"], name: "index_category_translation"
  end

  create_table "core_comments", force: :cascade do |t|
    t.string "title", limit: 50, default: ""
    t.text "body"
    t.string "commentable_type"
    t.integer "commentable_id"
    t.integer "user_id"
    t.string "role", default: "comments"
    t.integer "account_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "type"
    t.string "ancestry"
    t.integer "ancestry_depth", default: 0
    t.index ["account_id"], name: "index_core_comments_on_account_id"
    t.index ["commentable_id"], name: "index_core_comments_on_commentable_id"
    t.index ["commentable_type"], name: "index_core_comments_on_commentable_type"
    t.index ["user_id"], name: "index_core_comments_on_user_id"
  end

  create_table "core_custom_field_def_translations", force: :cascade do |t|
    t.integer "core_custom_field_def_id"
    t.string "locale"
    t.string "label"
    t.text "description"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["core_custom_field_def_id"], name: "core_custom_field_def_translations_index"
  end

  create_table "core_custom_field_defs", force: :cascade do |t|
    t.string "owner_type"
    t.integer "owner_id"
    t.string "name"
    t.string "field_type"
    t.integer "row_order"
    t.boolean "required", default: false
    t.string "properties", limit: 2048
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.index ["account_id"], name: "index_core_custom_field_defs_on_account_id"
    t.index ["owner_id", "owner_type"], name: "index_core_custom_field_defs_on_owner_id_and_owner_type"
  end

  create_table "core_custom_fields", force: :cascade do |t|
    t.string "owner_type"
    t.integer "owner_id"
    t.integer "custom_field_def_id"
    t.string "field_data", limit: 4096
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.index ["account_id"], name: "index_core_custom_fields_on_account_id"
    t.index ["custom_field_def_id"], name: "index_core_custom_fields_on_custom_field_def_id"
    t.index ["owner_id", "owner_type"], name: "index_core_custom_fields_on_owner_id_and_owner_type"
  end

  create_table "core_payment_histories", force: :cascade do |t|
    t.integer "owner_id"
    t.string "owner_type", limit: 30
    t.string "anchor_id", limit: 20
    t.string "order_ref"
    t.string "item_ref"
    t.string "item_name"
    t.integer "quantity"
    t.string "cost"
    t.string "discount"
    t.integer "total_cents"
    t.string "total_currency", limit: 3
    t.string "payment_method"
    t.datetime "payment_date", precision: nil
    t.string "bill_to_name"
    t.text "item_details"
    t.text "order_details"
    t.string "status"
    t.integer "user_profile_id"
    t.datetime "created_on", precision: nil
    t.integer "account_id"
    t.text "notify_data"
    t.string "transaction_id"
    t.index ["account_id"], name: "index_core_payment_histories_on_account_id"
    t.index ["anchor_id"], name: "index_payment_histories_on_anchor_id"
    t.index ["item_ref"], name: "index_payment_histories_on_item_ref"
    t.index ["order_ref"], name: "index_payment_histories_on_order_ref"
    t.index ["owner_id"], name: "index_payment_histories_on_owner_id"
    t.index ["owner_type"], name: "index_payment_histories_on_owner_type"
    t.index ["transaction_id"], name: "index_core_payment_histories_on_transaction_id"
    t.index ["user_profile_id"], name: "index_core_payment_histories_on_user_profile_id"
  end

  create_table "core_system_email_translations", force: :cascade do |t|
    t.integer "core_system_email_id"
    t.string "locale"
    t.string "subject"
    t.text "body"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["core_system_email_id"], name: "index_core_system_email_translations_on_core_system_email_id"
  end

  create_table "core_system_emails", force: :cascade do |t|
    t.string "email_type"
    t.string "emailable_type"
    t.integer "emailable_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.index ["account_id"], name: "index_core_system_emails_on_account_id"
    t.index ["emailable_id"], name: "index_core_system_emails_on_emailable_id"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at", precision: nil
    t.datetime "locked_at", precision: nil
    t.datetime "failed_at", precision: nil
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "email_newsletters", force: :cascade do |t|
    t.string "token"
    t.string "name"
    t.boolean "require_name", default: false
    t.boolean "require_country", default: false
    t.integer "subscribed_count", default: 0
    t.integer "unsubscribed_count", default: 0
    t.integer "cleaned_count", default: 0
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.string "type"
    t.string "mc_id"
    t.boolean "deleted", default: false
    t.index ["account_id"], name: "index_email_newsletters_on_account_id"
    t.index ["mc_id"], name: "index_email_newsletters_on_mc_id"
    t.index ["token"], name: "index_email_newsletters_on_token"
  end

  create_table "ems_registrations", force: :cascade do |t|
    t.integer "workshop_id"
    t.string "receipt_code", limit: 20
    t.boolean "receipt_requested"
    t.integer "workshop_price_id"
    t.integer "amount_paid_cents", default: 0
    t.integer "discount_value"
    t.boolean "discount_use_percent"
    t.datetime "checkin_at", precision: nil
    t.string "aasm_state", limit: 20
    t.datetime "process_changed_on", precision: nil
    t.datetime "user_updated_at", precision: nil
    t.datetime "archived_on", precision: nil
    t.datetime "confirmed_on", precision: nil
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.integer "lock_version", default: 0
    t.string "registered_locale", limit: 5
    t.integer "user_profile_id"
    t.string "amount_paid_currency"
    t.datetime "payment_reminder_sent_on", precision: nil
    t.string "uuid", limit: 36
    t.integer "comments_count"
    t.integer "payment_comment_id"
    t.text "payment_reminder_history"
    t.datetime "writtenoff_on", precision: nil
    t.date "payment_reminder_hold_until"
    t.index ["account_id"], name: "index_ems_registrations_on_account_id"
    t.index ["payment_comment_id"], name: "index_ems_registrations_on_payment_comment_id"
    t.index ["receipt_code"], name: "receipt_code_key"
    t.index ["user_profile_id"], name: "index_ems_registrations_on_user_profile_id"
    t.index ["uuid"], name: "index_ems_registrations_on_uuid"
    t.index ["workshop_id"], name: "index_ems_registrations_on_workshop_id"
    t.index ["workshop_price_id"], name: "index_ems_registrations_on_workshop_price_id"
  end

  create_table "ems_workshop_price_translations", force: :cascade do |t|
    t.integer "ems_workshop_price_id"
    t.string "locale"
    t.string "price_description"
    t.text "sub_description"
    t.text "payment_details"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["ems_workshop_price_id"], name: "index_ems_workshop_price_translations_on_ems_workshop_price_id"
  end

  create_table "ems_workshop_prices", force: :cascade do |t|
    t.integer "workshop_id"
    t.integer "price_cents"
    t.string "price_currency", limit: 10
    t.integer "alt1_price_cents"
    t.string "alt1_price_currency", limit: 10
    t.integer "alt2_price_cents"
    t.string "alt2_price_currency", limit: 10
    t.boolean "disabled", default: false, null: false
    t.date "valid_until"
    t.date "valid_starting_on"
    t.integer "total_available"
    t.integer "recurring_amount"
    t.integer "recurring_period"
    t.integer "recurring_number"
    t.integer "row_order"
    t.integer "account_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["account_id"], name: "index_ems_workshop_prices_on_account_id"
    t.index ["workshop_id"], name: "index_ems_workshop_prices_on_workshop_id"
  end

  create_table "ems_workshop_translations", force: :cascade do |t|
    t.integer "ems_workshop_id"
    t.string "locale"
    t.string "title"
    t.text "description"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.text "sidebar"
    t.text "summary"
    t.index ["ems_workshop_id"], name: "index_ems_workshop_translations_on_ems_workshop_id"
  end

  create_table "ems_workshops", force: :cascade do |t|
    t.string "slug"
    t.datetime "starting_on", precision: nil
    t.datetime "ending_on", precision: nil
    t.date "deadline_on"
    t.integer "registrations_count", default: 0
    t.integer "country_id"
    t.string "contact_email", limit: 60, default: ""
    t.string "contact_phone", limit: 20, default: ""
    t.string "info_url", default: ""
    t.boolean "require_review", default: false
    t.boolean "waitlisting", default: false
    t.text "closed_text"
    t.boolean "published"
    t.datetime "archived_on", precision: nil
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.boolean "require_account"
    t.boolean "require_address"
    t.boolean "require_photo"
    t.string "base_currency", limit: 3
    t.string "event_style", default: "workshop"
    t.integer "funding_goal_cents", default: 0
    t.boolean "payments_enabled"
    t.boolean "show_address"
    t.string "image"
    t.boolean "bcc_contact_email", default: false
    t.boolean "show_social_buttons", default: false
    t.string "header_accent_color"
    t.date "initial_payment_required_on"
    t.boolean "hide_dates", default: false
    t.index ["account_id"], name: "index_ems_workshops_on_account_id"
    t.index ["country_id"], name: "index_ems_workshops_on_country_id"
    t.index ["slug"], name: "workshopname_key"
  end

  create_table "fms_forum_sites", force: :cascade do |t|
    t.integer "account_id"
    t.boolean "enabled", default: false
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "forum_topics_count", default: 0
    t.integer "comments_count", default: 0
    t.integer "users_count", default: 0
    t.text "description"
    t.text "tagline"
    t.index ["account_id"], name: "index_fms_forum_sites_on_account_id"
  end

  create_table "fms_forum_topics", force: :cascade do |t|
    t.integer "account_id"
    t.integer "forum_id"
    t.integer "user_id"
    t.string "title"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "hits", default: 0
    t.boolean "sticky", default: false
    t.integer "comments_count", default: 0
    t.boolean "locked", default: false
    t.integer "last_forum_comment_id"
    t.datetime "last_updated_at", precision: nil
    t.integer "last_user_id"
    t.integer "forum_site_id"
    t.string "slug"
    t.index ["account_id", "forum_id", "slug"], name: "index_fms_forum_topics_on_account_id_and_forum_id_and_slug"
    t.index ["account_id", "last_updated_at", "forum_id"], name: "index_forum_topics_last_updated_at_forum_id"
    t.index ["account_id", "sticky", "last_updated_at", "forum_id"], name: "index_forum_topics_sticky_last_updated_at_forum_id"
    t.index ["forum_id"], name: "index_fms_forum_topics_on_forum_id"
    t.index ["forum_site_id"], name: "index_fms_forum_topics_on_forum_site_id"
    t.index ["last_forum_comment_id"], name: "index_fms_forum_topics_on_last_forum_comment_id"
    t.index ["last_user_id"], name: "index_fms_forum_topics_on_last_user_id"
    t.index ["user_id"], name: "index_fms_forum_topics_on_user_id"
  end

  create_table "fms_forums", force: :cascade do |t|
    t.integer "account_id"
    t.integer "forum_site_id"
    t.string "name"
    t.text "description"
    t.integer "forum_topics_count", default: 0
    t.integer "comments_count", default: 0
    t.integer "row_order"
    t.text "description_html"
    t.boolean "published", default: false
    t.string "slug"
    t.boolean "is_public", default: false
    t.boolean "requires_login", default: false
    t.integer "forum_category_id"
    t.boolean "requires_subscription", default: false
    t.integer "owner_id"
    t.string "owner_type"
    t.index ["account_id", "slug"], name: "index_fms_forums_on_account_id_and_slug"
    t.index ["forum_category_id"], name: "index_fms_forums_on_forum_category_id"
    t.index ["forum_site_id"], name: "index_fms_forums_on_forum_site_id"
    t.index ["owner_id"], name: "index_fms_forums_on_owner_id"
  end

  create_table "follows", force: :cascade do |t|
    t.string "followable_type", null: false
    t.integer "followable_id", null: false
    t.string "follower_type", null: false
    t.integer "follower_id", null: false
    t.boolean "blocked", default: false, null: false
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["followable_id", "followable_type"], name: "fk_followables"
    t.index ["follower_id", "follower_type"], name: "fk_follows"
  end

  create_table "globalize_countries", force: :cascade do |t|
    t.string "code", limit: 2
    t.string "english_name"
    t.string "date_format"
    t.string "currency_format"
    t.string "currency_code", limit: 3
    t.string "thousands_sep", limit: 2
    t.string "decimal_sep", limit: 2
    t.string "currency_decimal_sep", limit: 2
    t.string "number_grouping_scheme"
    t.string "continent"
    t.string "locale"
    t.index ["code"], name: "index_globalize_countries_on_code"
    t.index ["locale"], name: "index_globalize_countries_on_locale"
  end

  create_table "globalize_languages", force: :cascade do |t|
    t.string "iso_639_1", limit: 2
    t.string "iso_639_2", limit: 3
    t.string "iso_639_3", limit: 3
    t.string "rfc_3066"
    t.string "english_name"
    t.string "english_name_locale"
    t.string "english_name_modifier"
    t.string "native_name"
    t.string "native_name_locale"
    t.string "native_name_modifier"
    t.boolean "macro_language"
    t.string "direction"
    t.string "pluralization"
    t.string "scope", limit: 1
    t.index ["iso_639_1"], name: "index_globalize_languages_on_iso_639_1"
    t.index ["iso_639_2"], name: "index_globalize_languages_on_iso_639_2"
    t.index ["iso_639_3"], name: "index_globalize_languages_on_iso_639_3"
    t.index ["rfc_3066"], name: "index_globalize_languages_on_rfc_3066"
  end

  create_table "lms_course_translations", force: :cascade do |t|
    t.integer "course_id"
    t.string "locale"
    t.string "title"
    t.text "description"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "menutitle"
    t.index ["course_id"], name: "course_id_index"
  end

  create_table "lms_courses", force: :cascade do |t|
    t.integer "teacher_id"
    t.string "slug", limit: 191
    t.boolean "published"
    t.integer "row_order"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.boolean "teaser_only", default: false
    t.integer "account_id"
    t.boolean "require_subscription", default: true
    t.integer "owner_id"
    t.string "owner_type"
    t.boolean "is_public", default: false
    t.boolean "requires_login", default: false
    t.boolean "requires_subscription", default: false
    t.boolean "comments_allowed"
    t.index ["account_id"], name: "index_lms_courses_on_account_id"
    t.index ["slug"], name: "courseslug_key"
  end

  create_table "lms_lesson_pages", force: :cascade do |t|
    t.integer "lesson_id"
    t.string "slug", limit: 191
    t.boolean "published"
    t.integer "item_id"
    t.string "item_type"
    t.integer "row_order"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.integer "comments_count", default: 0
    t.index ["account_id"], name: "index_lms_lesson_pages_on_account_id"
    t.index ["slug"], name: "lessonpageslug_key"
  end

  create_table "lms_lesson_translations", force: :cascade do |t|
    t.integer "lesson_id"
    t.string "locale"
    t.string "title"
    t.text "description"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "menutitle"
    t.index ["lesson_id"], name: "lesson_id_index"
  end

  create_table "lms_lessons", force: :cascade do |t|
    t.integer "course_id"
    t.string "slug", limit: 191
    t.boolean "published"
    t.integer "row_order"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.index ["account_id"], name: "index_lms_lessons_on_account_id"
    t.index ["slug"], name: "lessonslug_key"
  end

  create_table "lms_teaching_translations", force: :cascade do |t|
    t.integer "teaching_id"
    t.string "locale"
    t.string "title"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "menutitle"
    t.text "content"
    t.index ["teaching_id"], name: "teaching_id_index"
  end

  create_table "lms_teachings", force: :cascade do |t|
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.index ["account_id"], name: "index_lms_teachings_on_account_id"
  end

  create_table "preferences", force: :cascade do |t|
    t.string "name", null: false
    t.string "owner_type", null: false
    t.integer "owner_id", null: false
    t.string "group_type"
    t.integer "group_id"
    t.string "value"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["group_id"], name: "index_preferences_on_group_id"
    t.index ["owner_id", "owner_type", "name", "group_id", "group_type"], name: "index_preferences_on_owner_and_name_and_preference", unique: true
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.integer "resource_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.index ["account_id"], name: "index_roles_on_account_id"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["resource_id"], name: "index_roles_on_resource_id"
  end

  create_table "taggings", force: :cascade do |t|
    t.integer "tag_id"
    t.string "taggable_type"
    t.integer "taggable_id"
    t.string "tagger_type"
    t.integer "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at", precision: nil
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "user_profiles", force: :cascade do |t|
    t.integer "user_id"
    t.string "public_name", limit: 50, default: ""
    t.string "first_name", limit: 50, default: ""
    t.string "last_name", limit: 50, default: ""
    t.string "address", limit: 70, default: ""
    t.string "address2", limit: 70, default: ""
    t.string "city", limit: 30, default: ""
    t.string "state", limit: 30, default: ""
    t.string "zipcode", limit: 10, default: ""
    t.integer "country_id", default: 0
    t.string "phone", limit: 20, default: ""
    t.string "cell", limit: 20, default: ""
    t.string "fax", limit: 20, default: ""
    t.string "workphone", limit: 20, default: ""
    t.string "website", limit: 50, default: ""
    t.string "gender", limit: 1, default: ""
    t.integer "status", default: 0
    t.integer "lock_version", default: 0
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.string "email"
    t.string "public_avatar"
    t.integer "public_avatar_file_size"
    t.string "public_avatar_content_type"
    t.string "private_avatar"
    t.integer "private_avatar_file_size"
    t.string "private_avatar_content_type"
    t.boolean "use_private_avatar_for_public", default: false
    t.string "favored_locale"
    t.index ["account_id"], name: "index_user_profiles_on_account_id"
    t.index ["country_id"], name: "index_user_profiles_on_country_id"
    t.index ["user_id"], name: "index_user_profiles_on_user_id"
  end

  create_table "user_site_profiles", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "last_access_at", precision: nil
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.string "uuid", limit: 40
    t.index ["account_id"], name: "index_user_site_profiles_on_account_id"
    t.index ["user_id"], name: "index_user_site_profiles_on_user_id"
    t.index ["uuid"], name: "index_user_site_profiles_on_uuid"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.integer "sign_in_count", default: 0
    t.datetime "current_sign_in_at", precision: nil
    t.datetime "last_sign_in_at", precision: nil
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at", precision: nil
    t.datetime "confirmation_sent_at", precision: nil
    t.string "unconfirmed_email"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "account_id"
    t.index ["account_id"], name: "index_users_on_account_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
  end

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id"
    t.string "foreign_key_name", limit: 191, null: false
    t.integer "foreign_key_id"
    t.index ["foreign_key_id"], name: "index_version_associations_on_foreign_key_id"
    t.index ["foreign_key_name", "foreign_key_id"], name: "index_version_associations_on_foreign_key"
    t.index ["version_id"], name: "index_version_associations_on_version_id"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object", limit: 1073741823
    t.datetime "created_at"
    t.text "object_changes", limit: 1073741823
    t.string "locale"
    t.integer "transaction_id"
    t.index ["item_id"], name: "index_versions_on_item_id"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
    t.index ["transaction_id"], name: "index_versions_on_transaction_id"
  end

  create_table "votes", force: :cascade do |t|
    t.string "votable_type"
    t.integer "votable_id"
    t.string "voter_type"
    t.integer "voter_id"
    t.boolean "vote_flag"
    t.string "vote_scope"
    t.integer "vote_weight"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope"
    t.index ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope"
  end

end
