# This migration comes from dm_forum (originally 20190122143740)
class RemoveDefaultForRowOrderFms < ActiveRecord::Migration[5.0]
  def change
    change_column_default(:fms_forums, :row_order, from: 0, to: nil)
  end
end
