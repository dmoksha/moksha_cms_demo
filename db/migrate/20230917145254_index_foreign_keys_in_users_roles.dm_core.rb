# This migration comes from dm_core (originally 20160821150133)
class IndexForeignKeysInUsersRoles < ActiveRecord::Migration[4.2]
  def change
    add_index :users_roles, :role_id
  end
end
