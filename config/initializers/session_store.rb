# frozen_string_literal: true

# Be sure to restart your server when you modify this file.

Rails.application.config.session_store :cookie_store, key: APPLICATION_SESSION_STORE_KEY, expire_after: 24.hours
