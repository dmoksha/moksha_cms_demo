# frozen_string_literal: true
# if we use config.force_ssl, we need to turn off the hsts subdomains.  Otherwise
# when going to local dev.oursite.net, we'll always get redirected to an SSL page.
# Rails.application.config.ssl_options = { hsts: { subdomains: false } }
