# frozen_string_literal: true

# Be sure to restart your server when you modify this file.

# Add new mime types for use in respond_to blocks:
# Mime::Type.register "text/richtext", :rtf

Mime::Type.register 'application/ogg', :ogg
Mime::Type.register 'application/ogg', :ogx
Mime::Type.register 'video/ogg', :ogv
Mime::Type.register 'audio/ogg', :oga
Mime::Type.register 'video/mp4', :mp4
Mime::Type.register 'video/mp4', :m4v
Mime::Type.register 'audio/mpeg', :mp3
Mime::Type.register 'audio/mpeg', :m4a
Mime::Type.register 'application/excel', :xls
Mime::Type.register 'video/webm', :webm
