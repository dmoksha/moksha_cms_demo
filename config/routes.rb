# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  scope ':locale' do
    devise_for :users, controllers: { registrations: 'dm_core/registrations', confirmations: 'dm_core/confirmations' }
  end

  #------------------------------------------------------------------------------

  themes_for_rails

  mount LetterOpenerWeb::Engine, at: '/letter_opener' if Rails.env.development?

  mount DmCore::Engine, at: '/'

  scope ':locale' do
    namespace :admin do
      # resources :users
    end

    get '/index', controller: 'dm_cms/pages', action: :show, slug: 'index', as: :index
  end

  mount DmLms::Engine,              at: '/'
  mount DmEvent::Engine,            at: '/'
  mount DmForum::Engine,            at: '/'
  mount DmNewsletter::Engine,       at: '/'
  mount DmCms::Engine,              at: '/'

  # use `get` instead of root to fix issue where sometimes '?locale=de' is appeneded
  get '/(:locale)', controller: 'dm_cms/pages', action: :show, slug: 'index', as: :root

  get '/protected_asset/*asset', controller: :protected_assets, action: :protected_asset, format: false, via: :get
end
