# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MokshaCms
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0
    config.active_support.cache_format_version = 7.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    #--- set the layout for the devise controllers
    config.to_prepare do
      Devise::SessionsController.layout 'devise'
      Devise::RegistrationsController.layout 'devise'
      Devise::ConfirmationsController.layout 'devise'
      Devise::UnlocksController.layout 'devise'
      Devise::PasswordsController.layout 'devise'
    end

    # config.active_job.queue_adapter = :delayed_job

    config.middleware.use Rack::Attack
  end
end

#------------------------------------------------------------------------------
module AssetsInitializers
  class Railtie < Rails::Railtie
    initializer 'assets_initializers.initialize_rails',
                group: :assets do |_app|
      require "#{Rails.root}/config/initializers/_themes_for_rails.rb"
      require "#{Rails.root}/config/initializers/account_initialization.rb"
    end
  end
end
