# frozen_string_literal: true

module DmCore
  class ConfirmationsController < Devise::ConfirmationsController
    include DmCore::Concerns::ConfirmationsController

    protected

    # The path used after confirmation.
    def after_confirmation_path_for(_resource_name, _resource)
      dm_cms.showpage_path(locale: DmCore::Language.locale, slug: 'confirmation_success')
    end
  end
end
